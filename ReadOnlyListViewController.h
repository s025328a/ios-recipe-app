//
//  ReadOnlyListViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadOnlyListViewController : UIViewController <UITableViewDataSource> {
    
    __weak IBOutlet UINavigationBar *navBar;
}

@property (nonatomic, strong) NSMutableArray *items;
//@property (nonatomic, strong) NSString *title;

@end
