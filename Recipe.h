//
//  Recipe.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ingredient, Tag, User;

@interface Recipe : NSManagedObject

@property (nonatomic, retain) NSString * method;
@property (nonatomic, retain) NSNumber * public;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSSet *tags;
@property (nonatomic, retain) NSSet *ingredients;
@end

@interface Recipe (CoreDataGeneratedAccessors)

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

- (void)addIngredientsObject:(Ingredient *)value;
- (void)removeIngredientsObject:(Ingredient *)value;
- (void)addIngredients:(NSSet *)values;
- (void)removeIngredients:(NSSet *)values;

@end
