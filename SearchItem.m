//
//  SearchItem.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "SearchItem.h"
#import "SearchCategory.h"


@implementation SearchItem

@dynamic name;
@dynamic searchCategory;

@end
