//
//  AddIngredientViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "AddIngredientViewController.h"

@interface AddIngredientViewController ()

@end

@implementation AddIngredientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ingredient = (Ingredient*)[NSEntityDescription insertNewObjectForEntityForName:@"Ingredient" inManagedObjectContext:self.managedObjectContext];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.measurements count];
    //return 5;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    measurementRow = row;
    Measurement *m = [self.measurements objectAtIndex:row];
    return m.type;
}

- (IBAction)cancelButtonPressed:(id)sender {
    [[self managedObjectContext] deleteObject:ingredient];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (IBAction)saveButtonPressed:(id)sender {
    ingredient.amount = [NSDecimalNumber decimalNumberWithString:amountText.text];
    ingredient.name = nameText.text;
    [ingredient setMeasurement:[self.measurements objectAtIndex:measurementRow]];
    [self.recipe addIngredientsObject:ingredient];
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers > 2) {
        IngredientsViewController *pvc = [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
        [pvc.table reloadData];
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)textFieldDoneEditing:(id)sender {
    [sender resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
