//
//  Recipe.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "Recipe.h"
#import "Ingredient.h"
#import "Tag.h"
#import "User.h"


@implementation Recipe

@dynamic method;
@dynamic public;
@dynamic summary;
@dynamic title;
@dynamic user;
@dynamic tags;
@dynamic ingredients;

@end
