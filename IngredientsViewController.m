//
//  IngredientsViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "IngredientsViewController.h"

@interface IngredientsViewController ()

@end

@implementation IngredientsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Measurement"];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if ([fetchedObjects count] > 0) {
        measurements = [fetchedObjects mutableCopy];
    }
    
    ingredients = [[self.recipe.ingredients allObjects] mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ingredients count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Ingredient *ingredient = (Ingredient*)[ingredients objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@", ingredient.amount, ingredient.measurement.type, ingredient.name];
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    path = indexPath;
}


 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
     if (editingStyle == UITableViewCellEditingStyleDelete) {
         // Delete the row from the data source
         Ingredient *ingredient = [ingredients objectAtIndex:indexPath.row];
         [self.managedObjectContext deleteObject:ingredient];
         [ingredients removeObject:ingredient];
         [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
     }
     else if (editingStyle == UITableViewCellEditingStyleInsert) {
         // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
 }
 

- (void)viewDidAppear:(BOOL)animated {
    ingredients = [[self.recipe.ingredients allObjects] mutableCopy];
    [self.table reloadData];
}

- (IBAction)addIngredient:(id)sender {
    //[self.table reloadData];
}

#pragma mark - Navigation

- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    AddIngredientViewController *pvc = [segue destinationViewController];
    
    [pvc setManagedObjectContext:self.managedObjectContext];
    pvc.measurements = measurements;
    pvc.recipe = self.recipe;
}


@end
