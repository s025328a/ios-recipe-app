//
//  Ingredient.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "Ingredient.h"
#import "Measurement.h"
#import "Recipe.h"


@implementation Ingredient

@dynamic amount;
@dynamic name;
@dynamic measurement;
@dynamic recipe;

@end
