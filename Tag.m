//
//  Tag.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "Tag.h"
#import "Recipe.h"
#import "SearchItem.h"


@implementation Tag

@dynamic recipe;
@dynamic searchItem;

@end
