//
//  AddTagViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/10/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "AddTagViewController.h"

@interface AddTagViewController ()

@end

@implementation AddTagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tag = (Tag*)[NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:self.managedObjectContext];
    
    SearchCategory *cat = [self.searchCategories objectAtIndex:0];
    
    [self loadSearchItems:cat];
    
    //searchItems = [[NSMutableArray alloc] init];
    //searchItems = [cat.searchItems mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (component) {
        case 0:
            return [self.searchCategories count];
            break;
        case 1:
            return [searchItems count];
            break;
    }    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        catIndex = row;
        SearchCategory *cat = [self.searchCategories objectAtIndex:row];
        /*searchItems = [cat.searchItems mutableCopy];
        [pickerView reloadComponent:1];*/
        [self loadSearchItems:cat];
    }
    else if (component == 1) {
        itemIndex = row;
    }
}

/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    //measurementRow = row;
    
}*/

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSAttributedString *title;
    if (component == 0) {
        SearchCategory *cat = [self.searchCategories objectAtIndex:row];
        title = [[NSAttributedString alloc] initWithString:cat.type attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        //return cat.type;
    }
    else if (component == 1) {
        /*SearchCategory *cat = [self.searchCategories objectAtIndex:row];
         NSMutableArray* items = [[NSMutableArray alloc] init];
         items = [cat.searchItems mutableCopy];*/
        SearchItem *item = [searchItems objectAtIndex:row];
        title = [[NSAttributedString alloc] initWithString:item.name attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        //return item.name;
    }
    //SearchCategory *cat = [self.searchCategories objectAtIndex:catIndex];
    return title;
}

- (void) loadSearchItems:(SearchCategory*)cat {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"SearchItem"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"searchCategory.type like %@", cat.type];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //NSLog(fetchedObjects);
    if (fetchedObjects != nil && [fetchedObjects count] > 0) {
        searchItems = [[NSMutableArray alloc] initWithArray:fetchedObjects];
        [picker reloadComponent:1];
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    if (tag != nil) {
        [[self managedObjectContext] deleteObject:tag];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (IBAction)saveButtonPressed:(id)sender {
    
    SearchItem *item = [searchItems objectAtIndex:itemIndex];
    [tag setSearchItem:item];
    [tag setRecipe:self.recipe];
    [self.recipe addTagsObject:tag];
    
    /*ingredient.amount = [NSDecimalNumber decimalNumberWithString:amountText.text];
    ingredient.name = nameText.text;
    [ingredient setMeasurement:[self.measurements objectAtIndex:measurementRow]];
    [self.recipe addIngredientsObject:ingredient];
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers > 2) {
        IngredientsViewController *pvc = [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
        [pvc.table reloadData];
    }*/
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
