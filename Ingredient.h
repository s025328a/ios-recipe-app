//
//  Ingredient.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Measurement, Recipe;

@interface Ingredient : NSManagedObject

@property (nonatomic, retain) NSDecimalNumber * amount;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Measurement *measurement;
@property (nonatomic, retain) Recipe *recipe;

@end
