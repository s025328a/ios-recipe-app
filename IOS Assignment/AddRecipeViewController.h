//
//  AddRecipeViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Recipe.h"
#import "Tag.h"
#import "Ingredient.h"
#import "Measurement.h"
#import "SearchItem.h"
#import "SearchCategory.h"
#import "IngredientsViewController.h"
#import "TagsViewController.h"

@interface AddRecipeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate> {
    //Recipe *recipe;
    NSMutableArray *menuOptions;
    NSIndexPath *path;
    __weak IBOutlet UITextField *titleText;
    __weak IBOutlet UITextView *summaryText;
    __weak IBOutlet UITextView *methodText;
    BOOL editing;
}

@property (nonatomic, strong) Recipe *recipe;


- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)saveButtonPressed:(id)sender;

- (IBAction)textFieldDoneEditing:(id)sender;

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
