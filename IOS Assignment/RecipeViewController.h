//
//  RecipeViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"
#import "Tag.h"
#import "SearchItem.h"
#import "SearchCategory.h"
#import "ReadOnlyListViewController.h"
#import "Ingredient.h"
#import "Measurement.h"
#import "AddRecipeViewController.h"

@interface RecipeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSIndexPath *path;
    __weak IBOutlet UILabel *summaryLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *methodLabel;
}

@property (nonatomic, strong) Recipe *recipe;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
