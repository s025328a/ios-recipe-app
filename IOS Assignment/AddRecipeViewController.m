//
//  AddRecipeViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "AddRecipeViewController.h"

@interface AddRecipeViewController ()

@end

@implementation AddRecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"Add Recipe"];
    
    [self loadMeasurements];
    
    if (self.recipe == nil) {
        self.recipe = (Recipe*)[NSEntityDescription insertNewObjectForEntityForName:@"Recipe" inManagedObjectContext:self.managedObjectContext];
    }
    else {
        editing = YES;
        titleText.text = self.recipe.title;
        summaryText.text = self.recipe.summary;
        methodText.text = self.recipe.method;
    }
}

- (void) loadTags {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Tag"];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if ([fetchedObjects count] > 0) {
        menuOptions = [fetchedObjects mutableCopy];
    }
}

-(void) saveRecipe {
    self.recipe.title = titleText.text;
    self.recipe.summary = summaryText.text;
    self.recipe.method = methodText.text;
    
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self saveRecipe];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Measurement"];
        
        NSError *error = nil;
        NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
        
        if ([fetchedObjects count] > 0) {
            
            for (Measurement* m in fetchedObjects) {
                NSLog(m.type);
            }
        }
        
        //[self.navigationController popViewControllerAnimated:NO];
    }
    [super viewWillDisappear:animated];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return [menuOptions count];
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //Tag *tag = (Tag*)[menuOptions objectAtIndex:indexPath.row];

    //cell.textLabel.text = [NSString stringWithFormat:@"Recipe: %@, Search Item: %@", tag.recipe.title, tag.searchItem.name];
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Ingredients";
            break;
        case 1:
            cell.textLabel.text = @"Tags";
            break;
    }
    
    //SearchItem* item = (SearchItem*)[menuOptions objectAtIndex:indexPath.row];
    
    //cell.textLabel.text = item.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    path = indexPath;
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"ShowIngredientsSegue" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"ShowTagsSegue" sender:self];
            break;
    }

}

- (void) loadMeasurements {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Measurement"];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:request error:&error];
    
    if (!([fetchedObjects count] > 0)) {
        NSMutableArray* types = [[NSMutableArray alloc] initWithObjects:@"grams", @"ml", @"cups", @"tins", @"whole", nil];
        for (NSString* type in types) {
            Measurement *measurement = (Measurement*)[NSEntityDescription insertNewObjectForEntityForName:@"Measurement" inManagedObjectContext:self.managedObjectContext];
            
            measurement.type = type;
        }
    }
    error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    if (!editing) {
        [[self managedObjectContext] deleteObject:self.recipe];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (IBAction)saveButtonPressed:(id)sender {
    [self saveRecipe];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (IBAction)textFieldDoneEditing:(id)sender {
    [sender resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if (path.row == 0) {
        IngredientsViewController *pvc = [segue destinationViewController];
    
        [pvc setManagedObjectContext:self.managedObjectContext];
        pvc.recipe = self.recipe;
    }
    else if (path.row == 1) {
        TagsViewController *pvc = [segue destinationViewController];
        
        [pvc setManagedObjectContext:self.managedObjectContext];
        pvc.recipe = self.recipe;
    }
}


@end
