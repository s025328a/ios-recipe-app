//
//  SearchItemMenuViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SearchItem.h"
#import "SearchCategory.h"
#import "RecipeListViewController.h"

@interface SearchItemMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *menuOptions;
    NSIndexPath *path;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) SearchCategory *category;

@end
