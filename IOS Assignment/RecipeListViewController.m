//
//  RecipeListViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/6/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "RecipeListViewController.h"

@interface RecipeListViewController ()

@end

@implementation RecipeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // load in search menu items using core data.
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Recipe"];
    
    if (self.filter != nil) {
        [fetchRequest setPredicate:self.filter];
    }
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //NSLog(fetchedObjects);
    if (fetchedObjects != nil && [fetchedObjects count] > 0) {
        recipes = [[NSMutableArray alloc] initWithArray:fetchedObjects];
    }
    [self.navigationItem setTitle:self.title];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [recipes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Recipe* r = (Recipe*)[recipes objectAtIndex:indexPath.row];
    cell.textLabel.text = r.title;
    
    /*for (Tag* tag in r.tags) {
        NSLog(tag.searchItem.name);
    }*/
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%@, Tag: %@", r.title, (Tag*)(r.tags[0]).searchItem.name];
    return cell;
}

/*- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    Recipe* r = (Recipe*)[recipes objectAtIndex:indexPath.row];
    
    for (Tag* tag in r.tags) {
        NSLog(tag.searchItem.name);
    }
}*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.filter == nil && editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Recipe *recipe = [recipes objectAtIndex:indexPath.row];
        [self.managedObjectContext deleteObject:recipe];
        [recipes removeObject:recipe];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    path = indexPath;
    [self performSegueWithIdentifier:@"ViewRecipeSegue" sender:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [table reloadData];
}

/*- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    RecipeViewController* pvc = [segue destinationViewController];
    pvc.managedObjectContext = [self managedObjectContext];
    pvc.recipe = (Recipe*)[recipes objectAtIndex:path.row];
}


@end
