//
//  MainMenuViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SearchCategory.h"
#import "AddRecipeViewController.h"
#import "SearchMenuViewController.h"
#import "RecipeListViewController.h"

@interface MainMenuViewController : UITableViewController {
    NSMutableArray *_menuOptions;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end
