//
//  RecipeListViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/6/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Recipe.h"
#import "Tag.h"
#import "SearchItem.h"

#import "RecipeViewController.h"
//#import <WindowsAzureMobileServices 2/WindowsAzureMobileServices.h>

@interface RecipeListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *recipes;
    NSIndexPath *path;
    __weak IBOutlet UITableView *table;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSPredicate *filter;

@end
