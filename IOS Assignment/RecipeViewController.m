//
//  RecipeViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    self.navigationController.title = self.recipe.title;
    titleLabel.text = self.recipe.title;
    summaryLabel.text = self.recipe.summary;
    methodLabel.text = self.recipe.method;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //Recipe* r = (Recipe*)[recipes objectAtIndex:indexPath.row];
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Ingredients";
            break;
        case 1:
            cell.textLabel.text = @"Tags";
            break;
    }
    
    //cell.textLabel.text = self.recipe.title;
    
    /*for (Tag* tag in r.tags) {
     NSLog(tag.searchItem.name);
     }*/
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%@, Tag: %@", r.title, (Tag*)(r.tags[0]).searchItem.name];
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    path = indexPath;
    [self performSegueWithIdentifier:@"ViewListSegue" sender:self];
}


/*-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self.navigationController popViewControllerAnimated:NO];
    }
    [super viewWillDisappear:animated];
}*/

- (void)viewDidAppear:(BOOL)animated {
    titleLabel.text = self.recipe.title;
    summaryLabel.text = self.recipe.summary;
    methodLabel.text = self.recipe.method;
}

- (IBAction)backButtonPressed:(id)sender {
    //[[self managedObjectContext] deleteObject:self.recipe];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (IBAction)editButtonPressed:(id)sender {
    //[self saveRecipe];
    [self performSegueWithIdentifier:@"EditRecipeSegue" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier  isEqual: @"EditRecipeSegue"]) {
        AddRecipeViewController *pvc = [segue destinationViewController];
        [pvc setManagedObjectContext:self.managedObjectContext];
        pvc.recipe = self.recipe;
    }
    else {
    
        ReadOnlyListViewController *pvc = [segue destinationViewController];
    
        NSMutableArray *list = [[NSMutableArray alloc] init];

        switch (path.row) {
            case 0:
                for (Ingredient* ing in self.recipe.ingredients) {
                    [list addObject:[NSString stringWithFormat:@"%@ %@ %@", ing.amount, ing.measurement.type, ing.name]];
                }
                pvc.items = list;
                pvc.title = @"Ingredients";
                break;
            case 1:
                for (Tag* tag in self.recipe.tags) {
                    [list addObject:[NSString stringWithFormat:@"%@: %@", tag.searchItem.searchCategory.type, tag.searchItem.name]];
                }
                pvc.items = list;
                pvc.title = @"Tags";
                break;
        }
    }
}


@end
