//
//  SearchItemMenuViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/8/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "SearchItemMenuViewController.h"

@interface SearchItemMenuViewController ()

@end

@implementation SearchItemMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"SearchItem"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"searchCategory.type like %@", self.category.type];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //NSLog(fetchedObjects);
    if (fetchedObjects != nil && [fetchedObjects count] > 0) {
        menuOptions = [[NSMutableArray alloc] initWithArray:fetchedObjects];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    SearchItem* item = (SearchItem*)[menuOptions objectAtIndex:indexPath.row];
    
    cell.textLabel.text = item.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    path = indexPath;
    [self performSegueWithIdentifier:@"RecipeListSegue" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    SearchItem *item = (SearchItem*)[menuOptions objectAtIndex:path.row];
    
    RecipeListViewController *pvc = [segue destinationViewController];
    
    [pvc setManagedObjectContext:self.managedObjectContext];
    pvc.filter = [NSPredicate predicateWithFormat:@"tags.searchItem.name CONTAINS[cd] %@", item.name];
}


@end
