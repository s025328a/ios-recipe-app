//
//  SearchMenuViewController.m
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/6/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import "SearchMenuViewController.h"

@interface SearchMenuViewController ()

@end

@implementation SearchMenuViewController

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [menuOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    SearchCategory* cat = (SearchCategory*)[menuOptions objectAtIndex:indexPath.row];
    
    cell.textLabel.text = cat.type;
    return cell;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    path = indexPath;
    
    SearchCategory* cat = (SearchCategory*)[menuOptions objectAtIndex:path.row];
    NSLog(cat.type);
    
    [self performSegueWithIdentifier:@"yourSegue" sender:self];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*path = indexPath;
    
    SearchCategory* cat = (SearchCategory*)[menuOptions objectAtIndex:path.row];
    NSLog(cat.type);*/
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     /*SearchCategory* cat = (SearchCategory*)[NSEntityDescription insertNewObjectForEntityForName:@"SearchCategory" inManagedObjectContext:self.managedObjectContext];
    
     SearchItem* item = (SearchItem*)[NSEntityDescription insertNewObjectForEntityForName:@"SearchItem" inManagedObjectContext:self.managedObjectContext];
    
    //SearchItem* item = [[SearchItem alloc] init];
    
     if (cat != nil) {
     cat.type = @"Meal";
     
     item.name = @"Breakfast";
         
         [cat addSearchItemsObject:item];
         item.searchCategory = cat;
         
     if (self.managedObjectContext != nil) {
     NSError *error = nil;
     if (![self.managedObjectContext save:&error]) {
     // Replace this implementation with code to handle the error appropriately.
     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
     NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
     abort();
     }
     }
     }*/
    
    //[self.navigationItem setTitle:@"Search Menu"];
    path = [[NSIndexPath alloc] init];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"SearchCategory"];

    NSError *error = nil;
    NSArray *fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    //NSLog(fetchedObjects);
    if (fetchedObjects != nil && [fetchedObjects count] > 0) {
        menuOptions = [[NSMutableArray alloc] initWithArray:fetchedObjects];
    }
    else {
        [self setUpInitialSearchCategories];
        fetchedObjects = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        menuOptions = [[NSMutableArray alloc] initWithArray:fetchedObjects];
    }
}

- (void)setUpInitialSearchCategories {
    
    [self addSearchCategory:@"Meal" withSearchItems:[NSMutableArray arrayWithObjects:@"Breakfast", @"Lunch", @"Starter", @"Main", @"Dessert", @"Snack", nil]];
    
    [self addSearchCategory:@"Main Ingredient" withSearchItems:[NSMutableArray arrayWithObjects:@"Fish & Seafood", @"Chicken", @"Vegetables", @"Beef", @"Pork", @"Lamb", @"Turkey", nil]];
    
    //[self addSearchCategory:@"Meal" withSearchItems:[NSMutableArray arrayWithObjects:@"Breakfast", @"Lunch", @"Starter", @"Main", @"Dessert", @"Snack", nil]];
    
    if (self.managedObjectContext != nil) {
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void) addSearchCategory:(NSString*)type withSearchItems:(NSMutableArray*)items {
    SearchCategory* cat = (SearchCategory*)[NSEntityDescription insertNewObjectForEntityForName:@"SearchCategory" inManagedObjectContext:self.managedObjectContext];
    
    if (cat != nil) {
        cat.type = type;
        
        for (NSString* name in items) {
            SearchItem* item = (SearchItem*)[NSEntityDescription insertNewObjectForEntityForName:@"SearchItem" inManagedObjectContext:self.managedObjectContext];
            
            item.name = name;
            item.searchCategory = cat;
            [cat addSearchItemsObject:item];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    SearchItemMenuViewController *pvc = [segue destinationViewController];
        
    [pvc setManagedObjectContext:self.managedObjectContext];
    pvc.category = (SearchCategory*)[menuOptions objectAtIndex:path.row];
}

@end
