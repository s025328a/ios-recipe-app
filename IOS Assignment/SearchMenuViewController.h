//
//  SearchMenuViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/6/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchCategory.h"
#import "SearchItem.h"
#import "SearchItemMenuViewController.h"
#import <CoreData/CoreData.h>

@interface SearchMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *menuOptions;
    NSIndexPath *path;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
