//
//  AddIngredientViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"
#import "Tag.h"
#import "SearchItem.h"
#import "SearchCategory.h"
#import "Ingredient.h"
#import "Measurement.h"
#import "IngredientsViewController.h"

@interface AddIngredientViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate> {
    Ingredient *ingredient;
    NSInteger measurementRow;
    __weak IBOutlet UITextField *amountText;
    __weak IBOutlet UITextField *nameText;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableArray *measurements;
@property (nonatomic, strong) Recipe *recipe;

@end
