//
//  TagsViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"
#import "Tag.h"
#import "SearchItem.h"
#import "SearchCategory.h"
#import "Ingredient.h"
#import "Measurement.h"
#import "AddTagViewController.h"

@interface TagsViewController : UIViewController {
    NSMutableArray *tags;
    NSMutableArray *searchCategories;
    __weak IBOutlet UITableView *table;
}

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) Recipe* recipe;

@end
