//
//  Tag.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Recipe, SearchItem;

@interface Tag : NSManagedObject

@property (nonatomic, retain) Recipe *recipe;
@property (nonatomic, retain) SearchItem *searchItem;

@end
