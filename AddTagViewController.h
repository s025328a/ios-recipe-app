//
//  AddTagViewController.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/10/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"
#import "Tag.h"
#import "SearchItem.h"
#import "SearchCategory.h"
#import "Ingredient.h"
#import "Measurement.h"

@interface AddTagViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate> {
    NSMutableArray *searchItems;
    NSInteger catIndex;
    NSInteger itemIndex;
    Tag *tag;
    __weak IBOutlet UIPickerView *picker;
}

@property (nonatomic, strong) NSMutableArray* searchCategories;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) Recipe *recipe;

@end
