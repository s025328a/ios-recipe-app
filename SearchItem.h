//
//  SearchItem.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SearchCategory;

@interface SearchItem : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) SearchCategory *searchCategory;

@end
