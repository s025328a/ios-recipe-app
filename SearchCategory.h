//
//  SearchCategory.h
//  IOS Assignment
//
//  Created by SILVERWOOD Benjamin on 5/9/15.
//  Copyright (c) 2015 SILVERWOOD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SearchItem;

@interface SearchCategory : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSSet *searchItems;
@end

@interface SearchCategory (CoreDataGeneratedAccessors)

- (void)addSearchItemsObject:(SearchItem *)value;
- (void)removeSearchItemsObject:(SearchItem *)value;
- (void)addSearchItems:(NSSet *)values;
- (void)removeSearchItems:(NSSet *)values;

@end
